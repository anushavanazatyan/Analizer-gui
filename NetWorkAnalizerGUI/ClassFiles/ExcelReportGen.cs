﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
//using System.Drawing;
namespace NetWorkAnalizerGUI
{
    public  class AnalizerData
    {
        public static List<LoopData> AnalizerLoop = new List<LoopData>();
    }
    public class LoopData
    {
        public System.Collections.Generic.List<double> data;// = new List<double>();
        public System.Collections.Generic.List<double> data2;

        public LoopData()
        {
            data = new List<double>();
            data2 = new List<double>();
        }

    }

    class ExcelReportGen
    {
        static Microsoft.Office.Interop.Excel.Application xlApp;
        static Workbook wb;
        static Worksheet ws;
        static Worksheet ws1;
        static Worksheet ws2;
        static Worksheet ws3;
        static Worksheet ws4;
        static Range aRange;

        public static void StartRep()
        {
            Init();
            WriteMinMax();
        }
        public static void  Init()
        {
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                MessageBox.Show("EXCEL could not be started. Check that your office installation and project references are correct.");
              //  return Task.FromResult<object>(null);
            }
        
            xlApp.Visible = true;
            

            wb = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            ws = (Worksheet)wb.Worksheets[1];

            ws1 = wb.Sheets.Add(After: wb.Sheets[wb.Sheets.Count]);
            ws2 = wb.Sheets.Add(After: wb.Sheets[wb.Sheets.Count]);
            ws3 = wb.Sheets.Add(After: wb.Sheets[wb.Sheets.Count]);
            ws4 = wb.Sheets.Add(After: wb.Sheets[wb.Sheets.Count]);
            ws.Name = "M1 All";
            ws1.Name = "M2 All";
            ws2.Name = "M1 Summary";
            ws3.Name = "M2 Summary";
            ws4.Name = "Overall";

            if (ws == null)
            {
                MessageBox.Show("Worksheet could not be created. Check that your office installation and project references are correct.");
            }

            // Select the Excel cells, in the range c1 to c7 in the worksheet.
            Range aRange = ws.get_Range((Range)ws.Cells[1, 3], (Microsoft.Office.Interop.Excel.Range)ws.Cells[7, 3]);

            if (aRange == null)
            {
                MessageBox.Show("Could not get a range. Check to be sure you have the correct versions of the office DLLs.");
            }

            // Fill the cells in the C1 to C7 range of the worksheet with the number 6.
            Object[] args = new Object[1];
            args[0] = 6;
            aRange.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, aRange, args);

            // Change the cells in the C1 to C7 range of the worksheet to the number 8.
            aRange.Value2 = 8;

            // Fix first row
            ws.Activate();
            ws.Application.ActiveWindow.SplitRow = 1;
            ws.Application.ActiveWindow.FreezePanes = true;
            // Now apply autofilter
         //   return Task.FromResult<object>(null);
        }

        public static  void WriteMinMax()
        {
            List<LoopData> _data = AnalizerData.AnalizerLoop;
            int pos = 1;
            for (int i = 0; i < _data.Count; i++)
            {
               
                ws.Cells[1, i + 2] = "Meaus " + (i + 1);
                for (int j = 0; j < _data[i].data.Count; j++)
                {
                    ws.Cells[j + 2, i + 2] = _data[i].data[j];
                }

                ws1.Cells[1, i + 2] = "Meaus " + (i + 1);
                for (int j = 0; j < _data[i].data2.Count; j++)
                {
                    ws1.Cells[j + 2, i + 2] = _data[i].data2[j];
                }
            }

            Range firstRow = (Range)ws.Rows[1];
            firstRow.AutoFilter(1,
                                Type.Missing,
                                XlAutoFilterOperator.xlAnd,
                                Type.Missing,
                                true);
            Range firstRow2 = (Range)ws1.Rows[1];
            firstRow2.AutoFilter(1,
                                Type.Missing,
                                XlAutoFilterOperator.xlAnd,
                                Type.Missing,
                                true);
           // Range coluRange = (Range) ws.Columns[2];
           // coluRange.

            for (int i = 0; i < _data.Count; i++)
            {
                var range = ws.get_Range((Range)ws.Cells[2, i+2], (Range)ws.Cells[(2 + _data[i].data.Count), i + 2 ]);
         //       Range range =  ws.get_Range((Range)ws.Cells[2,2], (Range)ws.Cells[60, 2]);
         //       Range aRange = ws.get_Range((Range)ws.Cells[1, 3], (Range)ws.Cells[7, 3]);
                DrowChart(range, 120, i * 210, @"Meausurment " + (i+1), ws2);
                pos = (i * 14)+3;
                
                ws2.Cells[pos + 1, 1] = "Min";
                ws2.Cells[pos + 1, 2] = _data[i].data.Min();

                ws2.Cells[pos + 2, 1] = "Max";
                ws2.Cells[pos + 2, 2] = _data[i].data.Max();

                ws2.Cells[pos + 3, 1] = "Avrage";
                ws2.Cells[pos + 3, 2] = _data[i].data.Average();// _data[i].data.Average();//range.Cells.Areas.Item.CalculateRowMajorOrder()//"AVRAGE('M{'" + i + "' Simmary})'!B11:B104)"; //_data[i].data.Average();
            }
            for (int i = 0; i < _data.Count; i++)
            {
                var range = ws1.get_Range((Range)ws1.Cells[2, i + 2], (Range)ws1.Cells[(2 + _data[i].data2.Count), i + 2]);
                //       Range range =  ws.get_Range((Range)ws.Cells[2,2], (Range)ws.Cells[60, 2]);
                //       Range aRange = ws.get_Range((Range)ws.Cells[1, 3], (Range)ws.Cells[7, 3]);
                DrowChart(range, 120, i * 210, @"Meausurment " + (i + 1), ws3);
                pos = (i * 14) + 3;

                ws3.Cells[pos + 1, 1] = "Min";
                ws3.Cells[pos + 1, 2] = _data[i].data2.Min();

                ws3.Cells[pos + 2, 1] = "Max";
                ws3.Cells[pos + 2, 2] = _data[i].data2.Max();

                ws3.Cells[pos + 3, 1] = "Avrage";
                ws3.Cells[pos + 3, 2] = _data[i].data2.Average();
            }
            writeAverages();
            aRange = ws.UsedRange;
            //    aRange = ws.get_Range(xlRange);
            aRange.Columns.AutoFit();
            aRange = ws1.UsedRange;
            aRange.Columns.AutoFit();
            aRange = ws2.UsedRange;
            aRange.Columns.AutoFit();
            aRange = ws3.UsedRange;
            aRange.Columns.AutoFit();
            aRange = ws4.UsedRange;
            aRange.Columns.AutoFit();
        }
        /*  public void WriteData(List<LoopData> _data, Microsoft.Office.Interop.Excel.Worksheet ws)
          {
           //   xlApp.Visible = true;
           //   Workbook wb = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
           //   Worksheet ws = (Worksheet)wb.Worksheets[1];
          //    Range row;
          //    Range UsedRange = ws.UsedRange;
          //    int lastUsedRow = UsedRange.Row + UsedRange.Rows.Count - 1;
         //     int lastUsedColumn = UsedRange.Column;
              for (int i = 0; i < _data.Count; i++)
              {
                  for (int j = 0; j < _data.Count; j++)
                  {
                      ws.Cells[i + 1, j+1] = _data[i].data[j];
                  }

              }

          }*/

        private static void DrowChart(object range, int posX, int posY, string graphTit, Worksheet ws1 /*, Microsoft.Office.Interop.Excel.Application application*/)
        {

          //  const string fileName = "D:\\Book1.xlsx";
          //  const string topLeft = "A1";
          //  const string bottomRight = "M1";
            const string graphTitle = "Graph Title";
            const string xAxis = "Time";
            const string yAxis = "Value";


            // Open Excel and get first worksheet.
            // var application = new Microsoft.Office.Interop.Excel.Application();
            //           application.Visible = true;
            //           Workbook wb = application.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            

            //     wb = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //      wb.Worksheets.Add(ws, null,1,Xl);
            //      Worksheet ws1 = (Worksheet)wb.Worksheets[2];
            
          //  Worksheet ws1 = (Worksheet)wb.Worksheets[1];
    //        var workbook = application.Workbooks.Add(fileName);
   //         var worksheet = workbook.Worksheets[1] as
  //              Microsoft.Office.Interop.Excel.Worksheet;

            // Add chart.
            var charts = ws1.ChartObjects() as
                Microsoft.Office.Interop.Excel.ChartObjects;
            var chartObject = charts.Add(posX, posY, 500, 200) as
                Microsoft.Office.Interop.Excel.ChartObject;
            var chart = chartObject.Chart;

            // Set chart range.
            
            chart.SetSourceData((Range)range);

            // Set chart properties.
            chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
            chart.ChartWizard(Source: range,
                Title: graphTit,
                CategoryTitle: xAxis,
                ValueTitle: yAxis);

            // Save.
         //   wb.Save();
        }
        public static void writeAverages()
        {
            for (int i = 0; i < AnalizerData.AnalizerLoop.Count; i++)
            {
                ws4.Cells[i+2, 1] = AnalizerData.AnalizerLoop[i].data.Average();
            }
            var range = ws4.get_Range((Range)ws4.Cells[2, 1], (Range)ws4.Cells[1 + AnalizerData.AnalizerLoop.Count, 1]);
            //       Range range =  ws.get_Range((Range)ws.Cells[2,2], (Range)ws.Cells[60, 2]);
            //       Range aRange = ws.get_Range((Range)ws.Cells[1, 3], (Range)ws.Cells[7, 3]);
            DrowChart(range, 70, 50, @"Marker 1 ", ws4 );
            int nextPlace = 14;
            for (int i = 0; i < AnalizerData.AnalizerLoop.Count; i++)
            {
                ws4.Cells[i + 2, nextPlace] = AnalizerData.AnalizerLoop[i].data2.Average();
            }
            var range2 = ws4.get_Range((Range)ws4.Cells[2, nextPlace], (Range)ws4.Cells[1 + AnalizerData.AnalizerLoop.Count, nextPlace]);
            //       Range range =  ws.get_Range((Range)ws.Cells[2,2], (Range)ws.Cells[60, 2]);
            //       Range aRange = ws.get_Range((Range)ws.Cells[1, 3], (Range)ws.Cells[7, 3]);
            DrowChart(range2, 700, 50, @"Marker 2 ", ws4);
        }
    }    
}
