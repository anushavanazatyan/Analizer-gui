﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetWorkAnalizerGUI
{
    partial class Form1
    {
        void MeausModeenabled(bool enabled)
        {
            //  Menu.
            if (enabled)
            {
                marker1ToolStripMenuItem.Enabled = true;
                markersToolStripMenuItem1.Enabled = true;
                btnStartMeaus.Text = @"Stop Meausurment";
            }
            else
            {
                marker1ToolStripMenuItem.Enabled = true;
                markersToolStripMenuItem1.Enabled = true;
                btnStartMeaus.Text = @"Start Meausurment";
            }
        }

        private Thread t, t2;
        public void GenerateReport()
        {
            if (t == null)
            {
                t = new Thread(ExcelReportGen.StartRep); //evry thread workes with a new progressBar

            }
            //if (t2 == null)
            //{
            //    t2 = new Thread(ExcelReportGen.WriteMinMax);
            //}
           t.Start();
            
        }
    }
}