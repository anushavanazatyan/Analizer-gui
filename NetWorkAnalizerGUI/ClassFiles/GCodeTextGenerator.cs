﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace NetWorkAnalizerGUI
{
    class GCodeTextGenerator
    {
        float number_of_moves = 0;
        float waitTime = 0;
        float speed = 0;
        float stepSize;
        public GCodeTextGenerator (float moves_num, float timeWait, float spd, float sizestep)
        {
            number_of_moves = moves_num;
            waitTime = timeWait;
            speed = spd;
            stepSize = sizestep;
        }
        public void writeTFile(string name)
        {
            
      //      string path = @"D:\Example.txt";
           string  path = name;
   //         path = Path.GetFullPath(path);

            try
            {
                
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                //  File.Create(path);
                fs.Flush();
                fs.Close();
                TextWriter tw = new StreamWriter(path);

                WriteGeneratedTxt(tw);
                tw.Close();
                MessageBox.Show(@" Save sucessfull in " + Path.GetFullPath(path));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
           



        }

        void WriteGeneratedTxt(TextWriter tw)
        {
            string startString = "g90g54g0g1f" + speed + Environment.NewLine;
            tw.Write(startString);
            float Y = 0;
            for (int i = 0; i<number_of_moves && (Y + stepSize) < 585; i++)
            {
                Y += stepSize;
                tw.Write(GenerateTxt(Y));
            }
        }
        string GenerateTxt(float Y)
        {
            
            string txt = "";
            
            txt += "G4";
            txt += " ";
            txt += "P";
            txt += waitTime.ToString();
            txt += Environment.NewLine;
            txt += "Y ";
            txt += Y.ToString();
   //         txt += "000000";
            txt += Environment.NewLine;
            return txt;

        }
    }
}
