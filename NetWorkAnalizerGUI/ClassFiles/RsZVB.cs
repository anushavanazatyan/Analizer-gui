﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Ivi.Visa.Interop;
using System.Threading;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Windows.Forms;

namespace NetWorkAnalizerGUI
{
    
    struct Options
    {
        public bool Enabled;
        public string command;
    };
    public struct myDouble
    {
        public double d;
        public int i;
    }
    public static class RsZVB
    {
        public const int CommandDelay = 90;
        static int threadId;
        public static Thread readThread;

        private static BackgroundWorker ss;

        // Create the delegate.
        //private static ThreadStart tsStart;

        // Initiate the asychronous call.
        //       Task aaTask = new Task();

        public static ConcurrentQueue<myDouble> cq = new ConcurrentQueue<myDouble>();
        public static ConcurrentQueue<myDouble> cq2 = new ConcurrentQueue<myDouble>();
        public static ConcurrentQueue<bool> cqStopThreads = new ConcurrentQueue<bool>();
        public static ConcurrentQueue<int> cqEndOfLoop = new ConcurrentQueue<int>();
        public static ConcurrentQueue<TimeSpan> cqTimerSmallLoop = new ConcurrentQueue<TimeSpan>();
        public static ConcurrentQueue<TimeSpan> cqTimerBigLoop = new ConcurrentQueue<TimeSpan>();
        public static ConcurrentQueue<bool> cqTimerstartStop = new ConcurrentQueue<bool>();
        public static ConcurrentQueue<int> cqStepNumber = new ConcurrentQueue<int>();
        public delegate void AsyncMethodCaller(int callDuration, out int threadId);

        private static Options[] _opt = new Options[10];
        static string _srcAddress = "TCPIP::172.28.28.68::INSTR";
        static Ivi.Visa.Interop.ResourceManager mngr;// = new Ivi.Visa.Interop.ResourceManager();
        static Ivi.Visa.Interop.FormattedIO488 instrument;// = new Ivi.Visa.Interop.FormattedIO488();
        private static Ivi.Visa.Interop.IMessage imsg;
        static string _returnOfCommand;
        static string _returnOfCommand2;
        static string _command = ":CALCulate:MARKer:Y?";
        static string _command2 = ":CALCulate:MARKer2:Y?";
        static string _trace1 = ":CALCulate:PARameter:SELect 'Trc1'";
        static string _trace2 = "::CALCulate:PARameter:SELect 'Trc2'";
        public static bool inited = false;

       public static bool initialiseDevice(string srcAddr, string com1, string com2, string meausNumStr, string sectorNumStr, 
           string delayBetweenStops, string sizeOfMove, string speedOfMove, bool disabled, int sleepTime, string trc1, string trc2)
       {
           if (srcAddr == null || com1 == null || meausNumStr == null || sectorNumStr == null ||
               delayBetweenStops == null)
           {
               return false;
           }
           float sizeOfMvflt;
           float speedOfmvflt;
           float.TryParse(sizeOfMove, out sizeOfMvflt);
           float.TryParse(speedOfMove, out speedOfmvflt);
           float allDelay = sizeOfMvflt * 60 / speedOfmvflt;
           int meausNum;
           int sectorNum;
           float stopDelay;
           int.TryParse(meausNumStr, out meausNum);
           int.TryParse(sectorNumStr, out sectorNum);
            float.TryParse(delayBetweenStops, out stopDelay);
           stopDelay *= 1000;
         //  int realSleep = stopDelay + (int)(allDelay * 1000);
            _srcAddress = srcAddr;
            Form2._meausNum = meausNum;
           if (com1 != null)
           {
               _command = com1;
           }
           if (com2 != null)
           {
               _command2 = com2;
           }
           if (trc1 != null)
           {
               _trace1 = trc1;
           }
           if (trc2 != null)
           {
               _trace2 = trc2;
           }


           ss = new BackgroundWorker();
            //   ss.DoWork += new DoWorkEventHandler(ss );
            readThread = new Thread(()=> StartReading(sectorNum , meausNum, stopDelay /*+ (allDelay*1000)*/, disabled, sleepTime));
            readThread.IsBackground = true;
           if (!inited)
               StartDevice();
               
           return true;
           
       }

        public static void StartDevice()
        {
            mngr = new Ivi.Visa.Interop.ResourceManager();
            instrument = new Ivi.Visa.Interop.FormattedIO488();
            imsg = (mngr.Open(_srcAddress, Ivi.Visa.Interop.AccessMode.NO_LOCK, 1000, "") as IMessage);
            instrument.IO = imsg;
            inited = true;
        }
        
        public static void SetOptions()
        {
            for (int i = 0; i < _opt.Length; i++)
            {
                if (_opt[i].Enabled)
                {
                    instrument.WriteString(_opt[i].command, true);
                }
            }
        }

        private static myDouble temp;
        private static myDouble temp2;
        private static int MeausSectorNum = 0;
        private  static bool stop = false;
        
        private static void StartReading(int nOfSector, int nofMeausperSector, float stopDelay, bool disabled, int sleepTime)
        {
            TimeSpan _smallLoopts;
            TimeSpan _bigLoopts;
            DateTime dt, dt2 = DateTime.Now;
            int counter = 0;
            stop = false;
            bool _FirsFinish = false;
            int num = 0;
            
            while (MeausSectorNum < nOfSector && !stop)
            {
                dt = DateTime.Now;
                _bigLoopts = DateTime.Now - dt2 ;
                
                counter++;
                if (cqStopThreads.Count > 0)
                    cqStopThreads.TryDequeue(out stop);
                if (stop) break;
                
                if (counter * 10 > stopDelay)
                {
                    cqTimerBigLoop.Enqueue(_bigLoopts);
                    counter = 0;
                    cqTimerstartStop.Enqueue(false);
                    #region loop
                    for (int i = 0; i < nofMeausperSector && !stop; i++)
                    {
                        dt2 = DateTime.Now;
                        _smallLoopts = DateTime.Now - dt;
                        cqTimerSmallLoop.Enqueue(_smallLoopts);
                        _FirsFinish = true;
                        if (!disabled)
                        {
                            try
                            {
                                instrument.WriteString(_trace1, true);
                                 Thread.Sleep(20);
                                instrument.WriteString(_command, true);
                                _returnOfCommand = instrument.ReadString();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                break;
                            }

                            try
                            {
                                instrument.WriteString(_trace2, true);
                                  Thread.Sleep(20);
                                instrument.WriteString(_command, true);
                                _returnOfCommand2 = instrument.ReadString();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                break;
                            }
                        }
                        else
                        {
                            try
                            {
                                instrument.WriteString(_command, true);
                                _returnOfCommand = instrument.ReadString();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                break;
                            }

                            try
                            {
                                instrument.WriteString(_command2, true);
                                _returnOfCommand2 = instrument.ReadString();
                            }
                            catch (Exception e)
                            {
                                MessageBox.Show(e.Message);
                                break;
                            }
                        }
                        temp.i = i;
                        double.TryParse(_returnOfCommand, out temp.d);
                        cq.Enqueue(temp);
                        temp2.i = i;
                        double.TryParse(_returnOfCommand2, out temp2.d);
                        cq2.Enqueue(temp2);

                        Thread.Sleep(sleepTime);
                        if (cqStopThreads.Count > 0 || stop)
                        {
                            cqStopThreads.TryDequeue(out stop);
                            if (stop) break;
                        }
                        
                        
                    }
                    #endregion
                    cqTimerstartStop.Enqueue(true);
                    
                    MeausSectorNum++;
                    cqStepNumber.Enqueue(MeausSectorNum);
                    if (_FirsFinish)
                    {
                        num++;
                        cqEndOfLoop.Enqueue(num);
                    }
                }
                if (stop) break;
                Thread.Sleep(10);
            }
        }
        public static void CloseDevice()
        {
            try

            {
                instrument.IO.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(instrument);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(mngr);
            }
            catch (Exception e)
            { }
        }
    }
}
