﻿namespace NetWorkAnalizerGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadOptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.markersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewGraphhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marker1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveConfiguration = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnStartMeaus = new System.Windows.Forms.Button();
            this.btnBrowseGcode = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnGcode = new System.Windows.Forms.Button();
            this.TBGCode_Path = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TBCSV_Path = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TBNum_of_Stops = new System.Windows.Forms.TextBox();
            this.TBSize_Of_Move = new System.Windows.Forms.TextBox();
            this.TBWait_TimeCNC = new System.Windows.Forms.TextBox();
            this.TBMove_Speed = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TBStop_TimeAnalizer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TBRead_Delay = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TBNum_Of_Meaus_Per_Stop = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.tBoxLoad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.TBRead_Command = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TBDevice_Name = new System.Windows.Forms.TextBox();
            this.TBRead_Command1 = new System.Windows.Forms.TextBox();
            this.btnReportGen = new System.Windows.Forms.Button();
            this.lableEnTraces = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TBtrace1 = new System.Windows.Forms.TextBox();
            this.TBtrace2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem1,
            this.viewGraphhToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(500, 20);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem,
            this.loadOptionToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 16);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.closeToolStripMenuItem.Text = "Save Option";
            // 
            // loadOptionToolStripMenuItem
            // 
            this.loadOptionToolStripMenuItem.Name = "loadOptionToolStripMenuItem";
            this.loadOptionToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadOptionToolStripMenuItem.Text = "Load Option";
            // 
            // optionsToolStripMenuItem1
            // 
            this.optionsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markersToolStripMenuItem1,
            this.displayToolStripMenuItem});
            this.optionsToolStripMenuItem1.Name = "optionsToolStripMenuItem1";
            this.optionsToolStripMenuItem1.Size = new System.Drawing.Size(59, 16);
            this.optionsToolStripMenuItem1.Text = "options";
            // 
            // markersToolStripMenuItem1
            // 
            this.markersToolStripMenuItem1.Name = "markersToolStripMenuItem1";
            this.markersToolStripMenuItem1.Size = new System.Drawing.Size(116, 22);
            this.markersToolStripMenuItem1.Text = "Markers";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onToolStripMenuItem,
            this.offToolStripMenuItem});
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.displayToolStripMenuItem.Text = "Display";
            // 
            // onToolStripMenuItem
            // 
            this.onToolStripMenuItem.Name = "onToolStripMenuItem";
            this.onToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.onToolStripMenuItem.Text = "On";
            // 
            // offToolStripMenuItem
            // 
            this.offToolStripMenuItem.Name = "offToolStripMenuItem";
            this.offToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.offToolStripMenuItem.Text = "Off";
            // 
            // viewGraphhToolStripMenuItem
            // 
            this.viewGraphhToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marker1ToolStripMenuItem});
            this.viewGraphhToolStripMenuItem.Name = "viewGraphhToolStripMenuItem";
            this.viewGraphhToolStripMenuItem.Size = new System.Drawing.Size(86, 16);
            this.viewGraphhToolStripMenuItem.Text = "View Graphh";
            // 
            // marker1ToolStripMenuItem
            // 
            this.marker1ToolStripMenuItem.Name = "marker1ToolStripMenuItem";
            this.marker1ToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.marker1ToolStripMenuItem.Text = "Markers";
            this.marker1ToolStripMenuItem.Click += new System.EventHandler(this.marker1ToolStripMenuItem_Click);
            // 
            // btnSaveConfiguration
            // 
            this.btnSaveConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSaveConfiguration.Location = new System.Drawing.Point(716, 305);
            this.btnSaveConfiguration.Name = "btnSaveConfiguration";
            this.btnSaveConfiguration.Size = new System.Drawing.Size(147, 27);
            this.btnSaveConfiguration.TabIndex = 27;
            this.btnSaveConfiguration.Text = "Save";
            this.btnSaveConfiguration.UseVisualStyleBackColor = true;
            this.btnSaveConfiguration.Click += new System.EventHandler(this.btnSaveConfiguration_Click);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label8, 2);
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(736, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 18);
            this.label8.TabIndex = 17;
            this.label8.Text = "Number of moves";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label7, 2);
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(738, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 18);
            this.label7.TabIndex = 11;
            this.label7.Text = "Size of move mm";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label6, 2);
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(725, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Wait Time Seconds";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label5, 2);
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(714, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Move Speed mm/min";
            // 
            // btnStartMeaus
            // 
            this.btnStartMeaus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartMeaus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnStartMeaus.Location = new System.Drawing.Point(869, 338);
            this.btnStartMeaus.Name = "btnStartMeaus";
            this.tableLayoutPanel1.SetRowSpan(this.btnStartMeaus, 4);
            this.btnStartMeaus.Size = new System.Drawing.Size(181, 146);
            this.btnStartMeaus.TabIndex = 26;
            this.btnStartMeaus.Text = "START Meausurments";
            this.btnStartMeaus.UseVisualStyleBackColor = true;
            this.btnStartMeaus.Click += new System.EventHandler(this.btnStartMeaus_Click);
            // 
            // btnBrowseGcode
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.btnBrowseGcode, 2);
            this.btnBrowseGcode.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBrowseGcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnBrowseGcode.Location = new System.Drawing.Point(503, 433);
            this.btnBrowseGcode.Name = "btnBrowseGcode";
            this.btnBrowseGcode.Size = new System.Drawing.Size(360, 24);
            this.btnBrowseGcode.TabIndex = 25;
            this.btnBrowseGcode.Text = "Browse Path to GCode";
            this.btnBrowseGcode.UseVisualStyleBackColor = true;
            this.btnBrowseGcode.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(503, 366);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(207, 28);
            this.button3.TabIndex = 24;
            this.button3.Text = "Browse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnGcode
            // 
            this.btnGcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGcode.Location = new System.Drawing.Point(869, 240);
            this.btnGcode.Name = "btnGcode";
            this.btnGcode.Size = new System.Drawing.Size(181, 30);
            this.btnGcode.TabIndex = 23;
            this.btnGcode.Text = "Generate G-Code File";
            this.btnGcode.UseVisualStyleBackColor = true;
            this.btnGcode.Click += new System.EventHandler(this.btnGcode_Click);
            // 
            // TBGCode_Path
            // 
            this.TBGCode_Path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.TBGCode_Path, 2);
            this.TBGCode_Path.Location = new System.Drawing.Point(3, 433);
            this.TBGCode_Path.Name = "TBGCode_Path";
            this.TBGCode_Path.Size = new System.Drawing.Size(494, 24);
            this.TBGCode_Path.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label10, 2);
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(3, 412);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(171, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Path to save G-code File";
            // 
            // TBCSV_Path
            // 
            this.TBCSV_Path.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.TBCSV_Path, 2);
            this.TBCSV_Path.Location = new System.Drawing.Point(3, 368);
            this.TBCSV_Path.Name = "TBCSV_Path";
            this.TBCSV_Path.Size = new System.Drawing.Size(494, 24);
            this.TBCSV_Path.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label9, 2);
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(3, 345);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(176, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = "Locaton to save Excel file";
            // 
            // TBNum_of_Stops
            // 
            this.TBNum_of_Stops.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TBNum_of_Stops.Location = new System.Drawing.Point(869, 208);
            this.TBNum_of_Stops.Name = "TBNum_of_Stops";
            this.TBNum_of_Stops.Size = new System.Drawing.Size(100, 24);
            this.TBNum_of_Stops.TabIndex = 18;
            // 
            // TBSize_Of_Move
            // 
            this.TBSize_Of_Move.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TBSize_Of_Move.Location = new System.Drawing.Point(869, 174);
            this.TBSize_Of_Move.Name = "TBSize_Of_Move";
            this.TBSize_Of_Move.Size = new System.Drawing.Size(100, 24);
            this.TBSize_Of_Move.TabIndex = 14;
            // 
            // TBWait_TimeCNC
            // 
            this.TBWait_TimeCNC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TBWait_TimeCNC.Location = new System.Drawing.Point(869, 139);
            this.TBWait_TimeCNC.Name = "TBWait_TimeCNC";
            this.TBWait_TimeCNC.Size = new System.Drawing.Size(100, 24);
            this.TBWait_TimeCNC.TabIndex = 13;
            // 
            // TBMove_Speed
            // 
            this.TBMove_Speed.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TBMove_Speed.Location = new System.Drawing.Point(869, 102);
            this.TBMove_Speed.Name = "TBMove_Speed";
            this.TBMove_Speed.Size = new System.Drawing.Size(100, 24);
            this.TBMove_Speed.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(265, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "time wait after meausurment (seconds)";
            // 
            // TBStop_TimeAnalizer
            // 
            this.TBStop_TimeAnalizer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBStop_TimeAnalizer.Location = new System.Drawing.Point(282, 174);
            this.TBStop_TimeAnalizer.Name = "TBStop_TimeAnalizer";
            this.TBStop_TimeAnalizer.Size = new System.Drawing.Size(215, 24);
            this.TBStop_TimeAnalizer.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(258, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "interval between meaus (milliseconds)";
            // 
            // TBRead_Delay
            // 
            this.TBRead_Delay.AllowDrop = true;
            this.TBRead_Delay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBRead_Delay.Location = new System.Drawing.Point(282, 139);
            this.TBRead_Delay.Name = "TBRead_Delay";
            this.TBRead_Delay.Size = new System.Drawing.Size(215, 24);
            this.TBRead_Delay.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "number of meausurments per one stop";
            // 
            // TBNum_Of_Meaus_Per_Stop
            // 
            this.TBNum_Of_Meaus_Per_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNum_Of_Meaus_Per_Stop.Location = new System.Drawing.Point(282, 102);
            this.TBNum_Of_Meaus_Per_Stop.Name = "TBNum_Of_Meaus_Per_Stop";
            this.TBNum_Of_Meaus_Per_Stop.Size = new System.Drawing.Size(215, 24);
            this.TBNum_Of_Meaus_Per_Stop.TabIndex = 0;
            // 
            // btnLoad
            // 
            this.btnLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLoad.Location = new System.Drawing.Point(503, 305);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(207, 27);
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // tBoxLoad
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tBoxLoad, 2);
            this.tBoxLoad.Dock = System.Windows.Forms.DockStyle.Top;
            this.tBoxLoad.Location = new System.Drawing.Point(3, 305);
            this.tBoxLoad.Name = "tBoxLoad";
            this.tBoxLoad.Size = new System.Drawing.Size(494, 24);
            this.tBoxLoad.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(273, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Save, Load Configuration File";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.19107F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.96234F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.83752F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 186F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tBoxLoad, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.btnLoad, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.TBNum_Of_Meaus_Per_Stop, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.TBRead_Delay, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TBStop_TimeAnalizer, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.TBMove_Speed, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.TBWait_TimeCNC, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.TBSize_Of_Move, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.TBNum_of_Stops, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.TBCSV_Path, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.TBGCode_Path, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.btnGcode, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.button3, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.btnBrowseGcode, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.btnStartMeaus, 4, 10);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnSaveConfiguration, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.menuStrip1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.TBRead_Command, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TBDevice_Name, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TBRead_Command1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnReportGen, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.lableEnTraces, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBox1, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.TBtrace1, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.TBtrace2, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label14, 3, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1053, 504);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(167, 18);
            this.label11.TabIndex = 29;
            this.label11.Text = "Read Param1,  Param 2";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TBRead_Command
            // 
            this.TBRead_Command.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBRead_Command.Location = new System.Drawing.Point(282, 64);
            this.TBRead_Command.Name = "TBRead_Command";
            this.TBRead_Command.Size = new System.Drawing.Size(215, 24);
            this.TBRead_Command.TabIndex = 30;
            this.TBRead_Command.Text = ":CALCulate:MARKer:Y?";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(273, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "Device name";
            // 
            // TBDevice_Name
            // 
            this.TBDevice_Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBDevice_Name.Location = new System.Drawing.Point(282, 27);
            this.TBDevice_Name.Name = "TBDevice_Name";
            this.TBDevice_Name.Size = new System.Drawing.Size(215, 24);
            this.TBDevice_Name.TabIndex = 32;
            this.TBDevice_Name.Text = "TCPIP::172.28.28.68::INSTR";
            // 
            // TBRead_Command1
            // 
            this.TBRead_Command1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TBRead_Command1.Enabled = false;
            this.TBRead_Command1.Location = new System.Drawing.Point(503, 64);
            this.TBRead_Command1.Name = "TBRead_Command1";
            this.TBRead_Command1.Size = new System.Drawing.Size(207, 24);
            this.TBRead_Command1.TabIndex = 33;
            this.TBRead_Command1.Text = ":CALCulate:MARKer2:Y?";
            // 
            // btnReportGen
            // 
            this.btnReportGen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReportGen.Location = new System.Drawing.Point(716, 366);
            this.btnReportGen.Name = "btnReportGen";
            this.btnReportGen.Size = new System.Drawing.Size(147, 29);
            this.btnReportGen.TabIndex = 34;
            this.btnReportGen.Text = "Ganarate Report";
            this.btnReportGen.UseVisualStyleBackColor = true;
            this.btnReportGen.Click += new System.EventHandler(this.button1_Click);
            // 
            // lableEnTraces
            // 
            this.lableEnTraces.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lableEnTraces.AutoSize = true;
            this.lableEnTraces.Location = new System.Drawing.Point(737, 67);
            this.lableEnTraces.Name = "lableEnTraces";
            this.lableEnTraces.Size = new System.Drawing.Size(104, 18);
            this.lableEnTraces.TabIndex = 35;
            this.lableEnTraces.Text = "disable Traces";
            this.lableEnTraces.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(869, 69);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 36;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 211);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 18);
            this.label13.TabIndex = 37;
            this.label13.Text = "Traces Names";
            // 
            // TBtrace1
            // 
            this.TBtrace1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TBtrace1.Location = new System.Drawing.Point(282, 207);
            this.TBtrace1.Name = "TBtrace1";
            this.TBtrace1.Size = new System.Drawing.Size(215, 24);
            this.TBtrace1.TabIndex = 38;
            this.TBtrace1.Text = ":CALCulate:PARameter:SELect \'Trc1\'";
            // 
            // TBtrace2
            // 
            this.TBtrace2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TBtrace2.Location = new System.Drawing.Point(503, 207);
            this.TBtrace2.Name = "TBtrace2";
            this.TBtrace2.Size = new System.Drawing.Size(207, 24);
            this.TBtrace2.TabIndex = 39;
            this.TBtrace2.Text = ":CALCulate:PARameter:SELect \'Trc2\'";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(744, 211);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 18);
            this.label14.TabIndex = 40;
            this.label14.Text = "Number of stops";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 504);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "CNCAnalizer Meausurment Beta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadOptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem markersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem offToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewGraphhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marker1ToolStripMenuItem;
        private System.Windows.Forms.Button btnSaveConfiguration;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tBoxLoad;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox TBNum_Of_Meaus_Per_Stop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TBRead_Delay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBStop_TimeAnalizer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBMove_Speed;
        private System.Windows.Forms.TextBox TBWait_TimeCNC;
        private System.Windows.Forms.TextBox TBSize_Of_Move;
        private System.Windows.Forms.TextBox TBNum_of_Stops;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TBCSV_Path;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TBGCode_Path;
        private System.Windows.Forms.Button btnGcode;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnBrowseGcode;
        private System.Windows.Forms.Button btnStartMeaus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TBRead_Command;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TBDevice_Name;
        private System.Windows.Forms.TextBox TBRead_Command1;
        private System.Windows.Forms.Button btnReportGen;
        private System.Windows.Forms.Label lableEnTraces;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TBtrace1;
        private System.Windows.Forms.TextBox TBtrace2;
        private System.Windows.Forms.Label label14;
    }
}

