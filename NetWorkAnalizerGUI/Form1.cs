﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Ivi.Visa.Interop;
using System.IO;
using System.Threading;

namespace NetWorkAnalizerGUI
{
    public partial class Form1 : Form
    {
        Form2 f2; 
        public Form1()
        {
            InitializeComponent();
            f2 = new Form2();
        }

        private void btnGcode_Click(object sender, EventArgs e)
        {
            float numofMeaus;
            float delayPerMeaus;
            float allMesDel;
 //           float.TryParse(TBRead_Delay.Text, out delayPerMeaus);
            float.TryParse(TBRead_Delay.Text, out delayPerMeaus);
            float.TryParse(TBNum_Of_Meaus_Per_Stop.Text, out numofMeaus);
            if (checkBox1.Checked)
            {
                allMesDel = numofMeaus * delayPerMeaus;
            }
            else
            {
                allMesDel = numofMeaus * delayPerMeaus + 43;
            }
            float spd, movesize, numOfMoves, waitTime; 
            float.TryParse(TBMove_Speed.Text, out spd);
            
            float.TryParse(TBSize_Of_Move.Text, out movesize);
           
            float.TryParse(TBNum_of_Stops.Text, out numOfMoves);
            float.TryParse(TBWait_TimeCNC.Text, out waitTime);

            GCodeTextGenerator gen = new GCodeTextGenerator(numOfMoves, waitTime/* + allMesDel/1000*/, spd, movesize);
            gen.writeTFile(TBGCode_Path.Text);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ff = new OpenFileDialog();
            ff.Filter = @"xml files (*.xml)|*.xml|All files (*.*)|*.*";
            ff.RestoreDirectory = true;
            if (ff.ShowDialog() == DialogResult.OK)
            {
                
                tBoxLoad.Text = ff.FileName;
                SaveLoadFiles.ReadSetXMLVal(ff.FileName);
                TBNum_Of_Meaus_Per_Stop.Text = XML_attrib.AllValues[4];
                TBNum_of_Stops.Text = XML_attrib.AllValues[5];
                TBRead_Delay.Text = XML_attrib.AllValues[6];
                TBStop_TimeAnalizer.Text = XML_attrib.AllValues[7];
                TBMove_Speed.Text = XML_attrib.AllValues[8];
                TBWait_TimeCNC.Text = XML_attrib.AllValues[9];
                TBSize_Of_Move.Text = XML_attrib.AllValues[10];
                TBCSV_Path.Text = XML_attrib.AllValues[11];
                TBGCode_Path.Text = XML_attrib.AllValues[12];
                TBtrace1.Text = XML_attrib.AllValues[13];
                TBtrace2.Text = XML_attrib.AllValues[14];
                checkBox1.Checked = XML_attrib.AllValues[15].Contains("1");
            }
            
        }

        private void btnStartMeaus_Click(object sender, EventArgs e)
        {
            if (RsZVB.readThread !=null && RsZVB.readThread.IsAlive)
            {
                RsZVB.cqStopThreads.Enqueue(true);
                MeausModeenabled(false);
            }
            else
            {
                if (RsZVB.initialiseDevice(TBDevice_Name.Text, TBRead_Command.Text, TBRead_Command1.Text,
                    TBNum_Of_Meaus_Per_Stop.Text, TBNum_of_Stops.Text, TBStop_TimeAnalizer.Text , TBSize_Of_Move.Text, 
                    TBMove_Speed.Text, checkBox1.Checked, Convert.ToInt32(TBRead_Delay.Text), TBtrace1.Text, TBtrace2.Text))
                {
                    RsZVB.readThread.Start();
                    MeausModeenabled(true);
                   
                    f2.Show();
                    f2.Focus();
                }
                
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog foldDialog = new FolderBrowserDialog {Description = @"Location"};

            if (foldDialog.ShowDialog() == DialogResult.OK)
            {
            }
           
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
          
            //         RsZVB.readThread.Abort();
            if (RsZVB.readThread != null && RsZVB.readThread.IsAlive)
            {
                RsZVB.cqStopThreads.Enqueue(true);
                MessageBox.Show("Please Stop device before closing");
                e.Cancel= true ;
                MeausModeenabled(false);
            }
            else
            {
    //            RsZVB.CloseDevice();
            }
      //      System.Threading.Thread.Sleep(50);

        }

        private void marker1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (f2.Visible)
            {
                f2.Focus();
            }
            else
            {
                f2.Show();
            }
            

        }

        private void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            SaveLoadFiles.pathToFile = tBoxLoad.Text;
            XML_attrib.AllValues[4] = TBNum_Of_Meaus_Per_Stop.Text;
            XML_attrib.AllValues[5] = TBNum_of_Stops.Text;
            XML_attrib.AllValues[6] = TBRead_Delay.Text;
            XML_attrib.AllValues[7] = TBStop_TimeAnalizer.Text;
            XML_attrib.AllValues[8] = TBMove_Speed.Text;
            XML_attrib.AllValues[9] = TBWait_TimeCNC.Text;
            XML_attrib.AllValues[10] = TBSize_Of_Move.Text;
            XML_attrib.AllValues[11] = TBCSV_Path.Text;
            XML_attrib.AllValues[12] = TBGCode_Path.Text;
            XML_attrib.AllValues[13] = TBtrace1.Text;
            XML_attrib.AllValues[14] = TBtrace2.Text;
            XML_attrib.AllValues[15] = Convert.ToInt32(checkBox1.Checked).ToString();
            SaveLoadFiles.SaveXml();
        }

        private void button4_Click(object sender, EventArgs e)
        {
             ExcelReportGen gg = new ExcelReportGen();
     //       gg.Init();
    //        gg.Test2();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExcelReportGen c = new ExcelReportGen();
            GenerateReport();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         //   t = new Thread(birinicBar); //evry thread workes with a new progressBar


         //   t2 = new Thread(ikinciBar);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            string cncWait = TBWait_TimeCNC.Text;
            float cncWaitInt = 0, numOfMeausInt = 0;
            
            string numOfMeaus = TBNum_Of_Meaus_Per_Stop.Text;
            float.TryParse(cncWait, out cncWaitInt);
            float.TryParse(numOfMeaus, out numOfMeausInt);
            
            if (checkBox1.Checked)
            {
                TBtrace1.Enabled = false;
                TBtrace2.Enabled = false;
                TBRead_Command1.Enabled = true;
                float cncWaitIntNew = cncWaitInt - (numOfMeausInt * 20) / 1000;
                TBWait_TimeCNC.Text = cncWaitIntNew.ToString();
            }
            else
            {
                TBtrace1.Enabled = true;
                TBtrace2.Enabled = true;
                TBRead_Command1.Enabled = false;
                float cncWaitIntNew = cncWaitInt + (numOfMeausInt * 20) / 1000;
                TBWait_TimeCNC.Text = cncWaitIntNew.ToString();
            }
        }
    }
}
                                        