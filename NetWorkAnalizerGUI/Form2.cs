﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetWorkAnalizerGUI
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            Config();
            timer1.Start();
        }

        void Config()
        {
    //        chart1.Series.Add("Marker1");
            chart1.Series["Marker1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["Marker2"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
        }
        private int num = 0;
        private bool timerOn;
        
        private myDouble _tmp;
        private myDouble _tmp2;
        private double _avrg;
        private int _num2 = 0;
        private int loopNum, stepNum =0;
        private TimeSpan tt1;
        private TimeSpan tt2;
        private TimeSpan tt3;
        private DateTime dtt;
        LoopData _temp = new LoopData();
        bool clear = false;
        public static int _meausNum = 10;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerOn)
            {
                tt3 = DateTime.Now - dtt;
                labelNewTimer.Text = string.Format("{0:D1}h:{1:D2}m:{2:D2}s:{3:D2}ms",
                        tt3.Hours,
                        tt3.Minutes,
                        tt3.Seconds,
                        tt3.Milliseconds / 10); 
            }
            labelQ1Count.Text = RsZVB.cq.Count.ToString();
            labelQ2Count.Text = RsZVB.cq2.Count.ToString();
            if (!RsZVB.cqTimerstartStop.IsEmpty)
            {
                RsZVB.cqTimerstartStop.TryDequeue(out timerOn);
                dtt = DateTime.Now;
            }
            if (!RsZVB.cqStepNumber.IsEmpty)
            {
                RsZVB.cqStepNumber.TryDequeue(out stepNum);
                labelStepNumber.Text = stepNum.ToString();
            }
            if (!RsZVB.cqTimerSmallLoop.IsEmpty)
            {
                RsZVB.cqTimerSmallLoop.TryDequeue(out tt1);
                labelTimer1.Text = string.Format("{0:D1}h:{1:D2}m:{2:D2}s:{3:D2}ms",
                        tt1.Hours,
                        tt1.Minutes,
                        tt1.Seconds,
                        tt1.Milliseconds / 10); 
            }
            if (!RsZVB.cqTimerBigLoop.IsEmpty)
            {
                RsZVB.cqTimerBigLoop.TryDequeue(out tt2);
                labelTimer2.Text = string.Format("{0:D1}h:{1:D2}m:{2:D2}s:{3:D2}ms",
                        tt2.Hours,
                        tt2.Minutes,
                        tt2.Seconds,
                        tt2.Milliseconds / 10);
            }
            //     AnalizerData Alldata = new AnalizerData();
            if (RsZVB.cq.Count>0)
            {
                if (clear)
                {
                    chart1.Series[0].Points.Clear();
                    chart1.Series[1].Points.Clear();
                    clear = false;
                }
                RsZVB.cq.TryDequeue(out _tmp);
                _temp.data.Add(_tmp.d);
                chart1.Series["Marker1"].Points.AddY(_tmp.d);
               
                label1.Text = @"Avrage = " + _temp.data.Average();
                label2.Text = @"Min = " + _temp.data.Min();
                label3.Text = @"Max = " + _temp.data.Max();
                



                if (RsZVB.cq2.Count > 0)
                {
                    RsZVB.cq2.TryDequeue(out _tmp2);
                    _temp.data2.Add(_tmp2.d);
                    chart1.Series["Marker2"].Points.AddY(_tmp2.d);
                
                    label4.Text = @"Avrage = " + _temp.data2.Average();
        //          min2 = (tmp2 < min2) ? min2 = tmp2: max2 = tmp2;
                    label5.Text = @"Min = " + _temp.data2.Min(); ;
                    label6.Text = @"Max = " + _temp.data2.Max();
                
                }
                if (_tmp.i == _meausNum-1 )
                {                   
                    clear = true;
                    //RsZVB.cqEndOfLoop.TryDequeue(out loopNum);
                    AnalizerData.AnalizerLoop.Add(_temp);
                    _temp = new LoopData();
                }
            }

            if (RsZVB.cqEndOfLoop.Count > 0)
            {
                //label4.Text = @"Avrage = " + _temp.data2.Average();
               
                //clear = true;
                //this.Refresh();
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }
    }
}
